﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    int score;
    //bool doubleEarnings;
    public Text scoreText;
    public Text hiScoreText;

    private void Start()
    {
        //PlayerPrefs.SetString("HiScore", "0");
        hiScoreText.text = GetHighScore();
    }



    public int GetScore()
    {
        return score;
    }

    public void SetScore(int i) {
        score = i;
        scoreText.text = score.ToString();
    }

    public string GetHighScore() {
        return PlayerPrefs.GetString("HiScore");
    }

    public void AddScore(int numToAdd, bool doubleEarnings)
    {
        int x = GetScore();
        int y = int.Parse(PlayerPrefs.GetString("HiScore"));
        /*if (doubleEarnings)
        {
            x = x + (numToAdd * 2);
        }*/

        SetScore (x + numToAdd);

        if (GetScore () > y)
        {           
            PlayerPrefs.SetString("HiScore", GetScore().ToString());
            hiScoreText.text = GetScore().ToString();            
        }
        
        scoreText.text = GetScore().ToString();
        /*if (x > y) {

        }*/
    }
}
