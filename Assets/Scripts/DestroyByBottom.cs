﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBottom : MonoBehaviour
{
    private GameManager gm;

    private void Start()
    {
        gm = GameObject.FindWithTag("GameController").GetComponent<GameManager>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Boundary")
        {
            return;
        }
        Debug.Log("Destroying  " + other);
        gm.gameOver();
        Destroy(other.gameObject);
        //Destroy(gameObject);
        
    }
}

