﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    private ScoreManager scoreM;

    private void Start()
    {
        scoreM = GameObject.FindWithTag("Player").GetComponent<ScoreManager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary")
        {
            return;
        }
        if (other.tag == "BottomBoundry") {
            return;
        }
        //add score
        scoreM.AddScore(100, false);
        Destroy(other.gameObject);
        //Destroy(gameObject);
    }
}
