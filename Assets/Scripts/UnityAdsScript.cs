﻿using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAdsScript : MonoBehaviour
{

    string gameId = "3072806";
    bool testMode = true;

    void Start()
    {
        Advertisement.Initialize(gameId, testMode);
    }
}
