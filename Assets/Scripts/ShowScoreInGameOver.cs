﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowScoreInGameOver : MonoBehaviour
{
    public Text hiScore;
    public Text score;

    private ScoreManager scoreM;

    private void Start()
    {
        scoreM = GameObject.FindWithTag("Player").GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        hiScore.text = scoreM.GetHighScore();
        score.text = scoreM.GetScore().ToString();
    }
}
