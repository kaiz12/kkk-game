﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class GameManager : MonoBehaviour
{

    /*
    TOPICS
    -   state machine
    -   Singeltons


    */

    /*FOR MENU*/
    public const int STATE_INIT = 0;
    public const int STATE_SPLASH = 1;
    public const int STATE_MENU = 2;
    public const int STATE_GAMEPLAY = 3;
    public const int STATE_IGM = 4;
    public const int STATE_EXIT = 5;
    public const int STATE_GAMEOVER = 6;



    private int currentState;

    private const int ACTION_INIT = 0;
    private const int ACTION_UPDATE = 1;

    private int currentAction;

    //FOR COIN

    public GameObject coin;
    List<GameObject> coinList; 
    public Vector3 spawnValue;
    public int coinCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    //FOR SCORE
    private ScoreManager scoreM;

    //FOR GAME
    bool isGamePlaying=false;


    void Start()
    {
        UpdateState(STATE_INIT, ACTION_INIT);
        //UpdateState(STATE_GAMEPLAY, STATE_INIT);
        // To Spawn Coins
        //StartCoroutine (SpawnVawes());
        scoreM = GameObject.FindWithTag("Player").GetComponent<ScoreManager>();
        coinList = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentAction == ACTION_UPDATE)
        {
            UpdateState(currentState, currentAction);
        }
    }

    private void UpdateState(int state, int action)
    {
        currentState = state;
        currentAction = action;

        switch (currentState)
        {
            case STATE_INIT:
                //Debug.Log("Initialize!");
                UpdateState(STATE_SPLASH, STATE_INIT);


                break;

            case STATE_SPLASH:
                if (action == ACTION_INIT)
                {

                    //Debug.Log("Show Splash!");
                    currentAction = ACTION_UPDATE;

                }
                else if (action == ACTION_UPDATE)
                {

                    // Show splash.
                    HUDManager.instance.screenSplash.SetActive(true);
                    HUDManager.instance.screenMenu.SetActive(false);
                    HUDManager.instance.screenGamePlay.SetActive(false);
                    HUDManager.instance.igm.SetActive(false);
                    HUDManager.instance.gameOver.SetActive(false);
                    //Debug.Log("Showing Splash...");

                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        UpdateState(STATE_MENU, STATE_INIT);
                    }
                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        //FOR PHONE we use Escape
                        //on back button we move to splash
                        ToExit();
                    }

                }
                break;

            case STATE_MENU:
                if (action == ACTION_INIT)
                {
                    //Debug.Log("Show Menu");
                    currentAction = ACTION_UPDATE;
                }
                else if (action == ACTION_UPDATE)
                {
                    HUDManager.instance.screenMenu.SetActive(true);
                    HUDManager.instance.screenSplash.SetActive(false);
                    HUDManager.instance.screenGamePlay.SetActive(false);
                    HUDManager.instance.igm.SetActive(false);
                    HUDManager.instance.gameOver.SetActive(false);
                    //Debug.Log("Showing Menu! Press 'P' for Play ...");
                    if (Input.GetKeyDown(KeyCode.P))
                    {
                        UpdateState(STATE_GAMEPLAY, STATE_INIT);
                    }
                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        //FOR PHONE we use Escape
                        //on back button we move to splash
                        UpdateState(STATE_SPLASH, STATE_INIT);
                    }
                }
                break;
            case STATE_GAMEPLAY:
                //StartCoroutine(SpawnVawes());
                if (action == ACTION_INIT)
                {
                    Debug.Log("You are playing an awsome game");
                    currentAction = ACTION_UPDATE;
                }
                else if (action == ACTION_UPDATE)
                {
                    HUDManager.instance.screenGamePlay.SetActive(true);
                    HUDManager.instance.screenMenu.SetActive(false);
                    HUDManager.instance.screenSplash.SetActive(false);
                    HUDManager.instance.igm.SetActive(false);
                    HUDManager.instance.gameOver.SetActive(false);
                    //Debug.Log("Still Playing? Press 'M' for In Game Menu ...");
                    if (Input.GetKeyDown(KeyCode.M))
                    {
                        UpdateState(STATE_IGM, STATE_INIT);
                    }
                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        //FOR PHONE we use Escape
                        //on back button we move to menu
                        UpdateState(STATE_MENU, STATE_INIT);
                    }
                }
                break;
            case STATE_IGM:
                if (action == ACTION_INIT)
                {
                    //Debug.Log("You are In Game Menu");
                    currentAction = ACTION_UPDATE;
                }
                else if (action == ACTION_UPDATE)
                {
                    HUDManager.instance.screenGamePlay.SetActive(false);
                    HUDManager.instance.screenMenu.SetActive(false);
                    HUDManager.instance.screenSplash.SetActive(false);
                    HUDManager.instance.igm.SetActive(true);
                    HUDManager.instance.gameOver.SetActive(false);
                    Debug.Log("Press 'P' for Play\nPress 'E' for Exit");

                    if (Input.GetKeyDown(KeyCode.P))
                    {
                        UpdateState(STATE_GAMEPLAY, STATE_INIT);

                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
#if UNITY_EDITOR
                        UnityEditor.EditorApplication.isPlaying = false;
#else
                        Application.Quit();
#endif
                    }
                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        //FOR PHONE we use Escape
                        //on back button we move to menu
                        UpdateState(STATE_GAMEPLAY, STATE_INIT);
                    }


                }
                break;
            case STATE_GAMEOVER:
                if (action == ACTION_INIT)
                {
                    currentAction = ACTION_UPDATE;
                    ShowRewardedAd();
                }
                else if (action == ACTION_UPDATE)
                {
                    DestroyCoins();
                    


                    HUDManager.instance.screenGamePlay.SetActive(false);
                    HUDManager.instance.screenMenu.SetActive(false);
                    HUDManager.instance.screenSplash.SetActive(false);
                    HUDManager.instance.igm.SetActive(false);
                    HUDManager.instance.gameOver.SetActive(true);
                    
                    //Debug.Log("Press 'P' for Play\nPress 'E' for Exit");

                    if (Input.GetKeyDown(KeyCode.P))
                    {
                        UpdateState(STATE_GAMEPLAY, STATE_INIT);
                        //actions for restart ...
                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        ToExit();
                    }
                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        ToExit();
                    }




                }
                break;
        }
    }

    //methods for Buttons
    public void SplashToMenu()
    {
        UpdateState(STATE_MENU, STATE_INIT);
        Advertisement.Show();
    }
    public void MenuToGamePlay()
    {
        UpdateState(STATE_GAMEPLAY, STATE_INIT);
        isGamePlaying = true;
        StartCoroutine(SpawnVawes());
    }
    public void GamePlayToIGM ()
    {

        UpdateState(STATE_IGM, STATE_INIT);
        isGamePlaying = false;
    }
    public void IGMToPlay()
    {
        isGamePlaying = true;
        UpdateState(STATE_GAMEPLAY, STATE_INIT);
    }
    public void Restart() {
        scoreM.SetScore(0);
        //isGamePlaying = false;
        isGamePlaying = true;
        UpdateState(STATE_GAMEPLAY, STATE_INIT);
        StartCoroutine(SpawnVawes());
    }

    public void IGMToExit() {
        isGamePlaying = false;
        DestroyCoins();
        ToExit();
    }

    public void ToExit() {
        isGamePlaying = false;
        DestroyCoins();
#if     UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void gameOver() {
        isGamePlaying = false;
        DestroyCoins();
        UpdateState(STATE_GAMEOVER, STATE_INIT);
    }

    private void DestroyCoins() {
        foreach (GameObject coin in coinList)
        {
            Destroy(coin);
        }
        coinList.Clear();
    }

    // Spawning coins
    //vaata üle
    IEnumerator SpawnVawes() {
        yield return new WaitUntil(()=>currentState==STATE_GAMEPLAY);
        while (isGamePlaying)
        {

            for (int i = 0; i < coinCount; i++)
            {
                yield return new WaitForSeconds(startWait);
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
                Quaternion spawnRotation = Quaternion.identity;
                coinList.Add((GameObject)Instantiate(coin, spawnPosition, spawnRotation));
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
        }
        
    }

    // ADD STUFF
    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = AdResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void AdResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                scoreM.AddScore(10000,false);
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }




}
